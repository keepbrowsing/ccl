@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dashboard - Sales Achievements</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                        <div class="container">
                            <div class="row">
                                <top-ten></top-ten>
                                <two-weeks-report></two-weeks-report>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
