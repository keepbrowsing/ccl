## About Carpet Call Dashboard

This is a test dashboard which is responsible for displaying the sales report and comparison of sales in last week vs this week.
## Installation Guide
```
npm install
php artisan migrate
php artisan db:seed --class=BranchSeeder
```

## Default Login Detail
- Username: themaheshe1@gmail.com
- Password: mahesh123#

## Notes
- Seeded working database has been attached , which makes easier to test.
