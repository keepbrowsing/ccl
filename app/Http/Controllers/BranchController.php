<?php

namespace App\Http\Controllers;

use App\Branch;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;

class BranchController extends Controller
{
    /**
     * @return JsonResponse
     */
    public function getTopTenBranch()
    {
        //Fetch top ten branches on the basis of sales(points)
        $sales = Branch::with('sales')
            ->leftJoin('sales','branches.id','=','sales.branch_id')
            ->selectRaw('branches.name,COALESCE(sum(sales.expenses),0) total_expenses, COALESCE(sum(sales.sales),0) total_sales')
            ->groupBy('branches.id')
            ->orderBy('total_sales','desc')
            ->take(10)
            ->get()->toArray();

        return response()
            ->json($sales);
    }

    /**
     * @return JsonResponse
     */
    public function getTwoWeeksReport()
    {
        //Fetch two weeks comparison report
        $salesLastWeek = Branch::with('sales')
            ->leftJoin('sales','branches.id','=','sales.branch_id')
            ->selectRaw('COALESCE(sum(sales.expenses),0) total_expenses, COALESCE(sum(sales.leads),0) total_leads, COALESCE(sum(sales.sales),0) total_sales')
            ->whereBetween('sales_date',[Carbon::now()->subDays(7)->startOfWeek(), Carbon::now()->subDays(7)->endOfWeek() ])
            ->first();

        $salesThisWeek = Branch::with('sales')
            ->leftJoin('sales','branches.id','=','sales.branch_id')
            ->selectRaw('COALESCE(sum(sales.expenses),0) total_expenses, COALESCE(sum(sales.leads),0) total_leads, COALESCE(sum(sales.sales),0) total_sales')
            ->whereBetween('sales_date',[Carbon::now()->startOfWeek(), Carbon::now() ])
            ->first();

        return response()
            ->json(
                [
                    'lastweek' => $salesLastWeek,
                    'thisweek' => $salesThisWeek,
                ]
            );
    }
}
