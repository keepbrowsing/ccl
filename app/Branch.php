<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    protected $table = 'branches';

    public function sales()
    {
        return $this->hasMany(Sales::class, 'branch_id');
    }
}
