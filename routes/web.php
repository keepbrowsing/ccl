<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

/** Restricted Area Requires Login*/
Route::prefix('api')->middleware('auth')->group(function () {
    Route::get('top-ten-branches', 'BranchController@getTopTenBranch')->name('branch.topten');
    Route::get('two-weeks-report', 'BranchController@getTwoWeeksReport')->name('branch.two_weeks_report');
});

Route::get('/', 'HomeController@index')->name('home');
