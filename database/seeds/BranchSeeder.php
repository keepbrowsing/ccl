<?php

use Illuminate\Database\Seeder;

class BranchSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Branch::class, 50)->create()->each(function ($branch) {
            $branch->sales()->save(factory(App\Sales::class)->make());
        });
    }
}
