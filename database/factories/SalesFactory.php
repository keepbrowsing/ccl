<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Sales;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Sales::class, function (Faker $faker) {
    return [
        'sales_date' => $faker->dateTimeBetween('-2 weeks', 'now'),
        'expenses' => $faker->randomFloat(2,0,2000),
        'leads' => $faker->numberBetween(0,20),
        'sales' => $faker->randomFloat(2,0, 5000),
    ];
});
